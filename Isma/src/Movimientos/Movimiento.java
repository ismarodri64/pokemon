package Movimientos;

public class Movimiento {
	
	private Tipos tipo;
	private CategoriaMov categoria;
	
	private int poder;
	private int pp;
	private float acierto;
	
	
	private String nombre;
	private String descripcion;
	
	public Movimiento(Tipos tipo, CategoriaMov categoria, int poder, float acierto, int pp, String nombre, String descripcion) {
		this.tipo = tipo;
		this.categoria = categoria;
		this.poder = poder;
		this.acierto = acierto;
		this.pp = pp;
		this.nombre = nombre;
		this.descripcion = descripcion;
	}
	public Tipos getType() {
		return tipo;
	}
	public CategoriaMov getCategory() {
		return categoria;
	}
	public int getPoder() {
		return poder;
	}
	public float getAcierto() {
		return acierto;
	}
	public int getPP() {
		return pp;
	}
	public String getNomre() {
		return nombre;
	}
	public String getDescripcion() {
		return descripcion;
	
	}
}
