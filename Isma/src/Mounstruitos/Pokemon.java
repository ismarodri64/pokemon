package Mounstruitos;

import Movimientos.Movimiento;
import Movimientos.Tipos;

public class Pokemon {

		int pokedexId;
		String nom;
		int hp;
		int def;
		int spd;
		Tipos tipo;
		Movimiento[] ataques = new Movimiento[4];
		
		
	Pokemon(int pokedexId, String nom, int hp, int def, int spd, Tipos tipo, Movimiento[] ataques) {
		super();
		this.pokedexId = pokedexId;
		this.nom = nom;
		this.hp = hp;
		this.def = def;
		this.spd = spd;
		this.tipo = tipo;
		this.ataques = ataques;
		}
	
	
}
