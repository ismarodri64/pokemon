package Game;

import java.awt.Color;
import java.util.ArrayList;

import Core.Field;
import Core.Sprite;
import Core.Window;
import Mounstruitos.Pokemon;
import Movimientos.CategoriaMov;
import Movimientos.Movimiento;
import Movimientos.Tipos;

public class Juego {
	
	static int size = 5;
	static int sizeMap = 10;
	
	static int cont=0;
	static Field f = new Field();
	static Window w = new Window(f);
	static Personaje pj = new Personaje("quinoa", 100, 100, 121+size, 124+size, "quinoa.png");
    static Mapa mp = new Mapa("Ciudad", 0+sizeMap, 0+sizeMap, 1024+sizeMap, 1024+sizeMap, "Pueblo1.png");
    static ColPared casaAmarilla = new ColPared("casa amarilla", 161+sizeMap, 84+sizeMap, 246+sizeMap, 191+sizeMap, "");
    static Hierba hierba = new Hierba("hierba", 134, 200, 160, 229, "");
    static Pokeluca pokeluca = new Pokeluca("pokeluca", 382, 324, 408, 353,"MasterLucario.png");
    
    
 
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		imput();
		pj.x1 = 274;
		pj.x2 = pj.x1+(21+size);
		pj.y1 = 192;
		pj.y2 = pj.y1+24+size;
		pokeluca.terrain = true;
		pokeluca.MARGIN= 0;
		casaAmarilla.terrain = true;
		casaAmarilla.MARGIN= 0;
		
		Movimiento esferaaural = new Movimiento(Tipos.LUCHA,CategoriaMov.FISICO,80,98,12,"Esfera Aural","");
		Movimiento[] ataques = {esferaaural};
		Pokemon p1 = new Pokemon(448, "Lucario", 100, 70, 90,Tipos.LUCHA,98);

		
	
		while(true) {
			
			ArrayList<Sprite> sprites = new ArrayList<>();
			sprites.add(mp);
			sprites.add(pj);
			sprites.add(casaAmarilla);
			sprites.add(hierba);
			sprites.add(pokeluca);
			f.draw(sprites);
			
			pj.pjInput();
			
			if(pj.collidesWith(hierba)) {
				System.out.println("AAAAAAAAAAAAAAAAAAA");
			}
			checkForPokeballs();
			
			for (int i = 0; i < sprites.size(); i++) {
				if(pj.collidesWith(sprites.get(i))) { //AQUI MIRA SI TU PERSONAJE COLISIONA CON OTRO SPRITE
					if(sprites.get(i).terrain == true) { //MIRA SI EL SPRITE QUE HA COLISIONADO TU PERSONAJE ES DE TIPO TERRENO
						if(pj.headOn(sprites.get(i))) {
							System.out.println("aAAAAAAAAAAAA");
							pj.y1+= pj.speed;
							pj.y2+= pj.speed;
							f.scroll(0, -pj.speed);
						}
						
						if(pj.leftOn(sprites.get(i))) {
							System.out.println("aaaaaaaaaaaaaa");
							pj.x1+= pj.speed;
							pj.x2+= pj.speed;
							f.scroll(-pj.speed, 0);
							
						}
						
						if(pj.rightOn(sprites.get(i))) {
							pj.x1-= pj.speed;
							pj.x2-= pj.speed;
							f.scroll(pj.speed, 0);
						}
						if(pj.stepsOn(sprites.get(i))) {
							pj.y1-= pj.speed;
							pj.y2-= pj.speed;
							f.scroll(0,pj.speed);
						}
							
					}
						
				}
				
			}

			
			
			Thread.sleep(33);
		}
	}



	private static void checkForPokeballs() {
		Sprite collision;
		switch (pj.mirandoHacia) {
		case 's':
			collision = new Sprite("coll", pj.x1, pj.y2+1, pj.x2, pj.y2+8, "Pueblo1.png");
			for(Sprite coll : collision.collidesWithField(f)){
				if(coll instanceof Pokeluca) {
					System.out.println("--------------" + coll);
					//TODO darle un pokemon
				}
			}
			
			break;
		case 'w':
			collision = new Sprite("coll", pj.x1, pj.y1-8, pj.x2, pj.y1-1, "Pueblo1.png");
			for(Sprite coll : collision.collidesWithField(f)){
				if(coll instanceof Pokeluca) {
					System.out.println("--------------" + coll);
					//TODO darle un pokemon
				}
			}
			
			break;
		case 'a':
			collision = new Sprite("coll", pj.x1-9, pj.y1, pj.x1-1, pj.y2, "Pueblo1.png");
			for(Sprite coll : collision.collidesWithField(f)){
				if(coll instanceof Pokeluca) {
					System.out.println("--------------" + coll);
					//TODO darle un pokemon
				}
			}
			
			break;
		case 'd':
			collision = new Sprite("coll", pj.x2+1, pj.y1, pj.x2+9, pj.y2, "Pueblo1.png");
			for(Sprite coll : collision.collidesWithField(f)){
				if(coll instanceof Pokeluca) {
					System.out.println("--------------" + coll);
					//TODO darle un pokemon
				}
			}


		default:
			break;
		}
		
	}



	private static void imput() {
		// TODO Auto-generated method stub
			if (w.getPressedKeys().contains('a') || w.getPressedKeys().contains('d') || w.getPressedKeys().contains('w')
					|| w.getPressedKeys().contains('s')) {
				if (hierba.firstCollidesWithField(f) != null) {
					cont++;
					System.out.println("He colisionado con "+hierba.firstCollidesWithField(f).name+" numero de colision: "+cont);
					System.out.println("porcentaje de colision "+hierba.collidesWithPercent(hierba.firstCollidesWithField(f)));
				}

			}
		}

}

