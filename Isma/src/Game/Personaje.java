package Game;

import java.util.HashSet;

import Core.Sprite;

public class Personaje extends Sprite {
	
	int speed = 4;
	public char mirandoHacia = 's';

	public Personaje(String name, int x1, int y1, int x2, int y2, String path) {
		// TODO Auto-generated constructor stub
		super(name, x1, y1, x2, y2, path);
	}
	
	public void pjInput() {
		HashSet<Character> list = (HashSet<Character>) Juego.w.getPressedKeys();
		System.out.println(x1+" "+y1+" : "+x2+" "+y2);
		System.out.println(list);
		
		
		if(list.contains('a')) {
			x1 -= speed;
			x2 -= speed;
			Juego.pj.changeImage("izqstatic.png");
			if(x1>400) {
				Juego.f.lockScroll(this, Juego.w);
			}else {
				Juego.f.lockScroll=false;
				//Juego.f.scroll(speed, 0);
				
			}
			mirandoHacia = 'a';
				
		}
		if(list.contains('d')) {
			x1 += speed;
			x2 += speed;
			Juego.pj.changeImage("der.png");
			if(x1>400) {
				Juego.f.lockScroll(this, Juego.w);
			}else {
				Juego.f.lockScroll=false;
				//Juego.f.scroll(speed, 0);
				
			}
			mirandoHacia = 'd';
		}
		if(list.contains('w')) {
			y1 -= speed;
			y2 -= speed;
			Juego.pj.changeImage("arriba.png");
			Juego.f.scroll(0, speed);
			mirandoHacia = 'w';
		}
		if(list.contains('s')) {
			y1 += speed;
			y2 += speed;
			Juego.pj.changeImage("quinoa.png");
			Juego.f.scroll(0, -speed);
			mirandoHacia = 's';
		}
		if(list.contains('p')) {
			speed = 6;
		}
		else {
			speed = 4;
		}
			}
	}


